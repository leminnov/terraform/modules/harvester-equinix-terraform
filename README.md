## Harvester on Equinix

Simple example using the terraform equinix provider to create a multi node harvester cluster.

The user needs to provide two environment variables

`METAL_AUTH_TOKEN` API token to access your Equinix account

`TF_VAR_project_name` Terraform variable to identify project in your Equinix account. You can overwrite any values in variables.tf by using .tfvars files or [other means](https://www.terraform.io/language/values/variables#assigning-values-to-root-module-variables)

By default the module will create a 2 node Harvester cluster.

The Harvester console can be accessed using an Elastic IP created by the sample.

A random token and password will be generated for your example.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_equinix"></a> [equinix](#requirement\_equinix) | 1.14.1 |
| <a name="requirement_rancher2"></a> [rancher2](#requirement\_rancher2) | 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_equinix"></a> [equinix](#provider\_equinix) | 1.14.1 |
| <a name="provider_rancher2"></a> [rancher2](#provider\_rancher2) | 2.0.0 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [equinix_metal_device.join](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_device) | resource |
| [equinix_metal_device.seed](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_device) | resource |
| [equinix_metal_ip_attachment.first_address_assignment](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_ip_attachment) | resource |
| [equinix_metal_port_vlan_attachment.vlan_attach_join](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_port_vlan_attachment) | resource |
| [equinix_metal_port_vlan_attachment.vlan_attach_seed](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_port_vlan_attachment) | resource |
| [equinix_metal_reserved_ip_block.harvester_vip](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_reserved_ip_block) | resource |
| [equinix_metal_spot_market_request.join_spot_request](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_spot_market_request) | resource |
| [equinix_metal_spot_market_request.seed_spot_request](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_spot_market_request) | resource |
| [equinix_metal_vlan.vlans](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/resources/metal_vlan) | resource |
| [rancher2_cluster.rancher_cluster](https://registry.terraform.io/providers/rancher/rancher2/2.0.0/docs/resources/cluster) | resource |
| [random_password.password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.token](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [equinix_metal_device.join_devices](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/data-sources/metal_device) | data source |
| [equinix_metal_device.seed_device](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/data-sources/metal_device) | data source |
| [equinix_metal_ip_block_ranges.address_block](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/data-sources/metal_ip_block_ranges) | data source |
| [equinix_metal_project.project](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/data-sources/metal_project) | data source |
| [equinix_metal_spot_market_request.join_req](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/data-sources/metal_spot_market_request) | data source |
| [equinix_metal_spot_market_request.seed_req](https://registry.terraform.io/providers/equinix/equinix/1.14.1/docs/data-sources/metal_spot_market_request) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_billing_cylce"></a> [billing\_cylce](#input\_billing\_cylce) | n/a | `string` | `"hourly"` | no |
| <a name="input_harvester_version"></a> [harvester\_version](#input\_harvester\_version) | n/a | `string` | `"v1.1.2-rc7"` | no |
| <a name="input_hostname_prefix"></a> [hostname\_prefix](#input\_hostname\_prefix) | n/a | `string` | `"harvester-pxe"` | no |
| <a name="input_ipxe_script"></a> [ipxe\_script](#input\_ipxe\_script) | n/a | `string` | `"https://raw.githubusercontent.com/rancherlabs/harvester-equinix-terraform/main/ipxe/ipxe-"` | no |
| <a name="input_max_bid_price"></a> [max\_bid\_price](#input\_max\_bid\_price) | Maximum bid price for spot request. | `string` | `"0.00"` | no |
| <a name="input_metro"></a> [metro](#input\_metro) | n/a | `string` | `"SG"` | no |
| <a name="input_node_count"></a> [node\_count](#input\_node\_count) | n/a | `string` | `"2"` | no |
| <a name="input_num_of_vlans"></a> [num\_of\_vlans](#input\_num\_of\_vlans) | Number of VLANs to be created | `number` | `0` | no |
| <a name="input_plan"></a> [plan](#input\_plan) | n/a | `string` | `"c3.small.x86"` | no |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | n/a | `string` | `""` | no |
| <a name="input_rancher_access_key"></a> [rancher\_access\_key](#input\_rancher\_access\_key) | Rancher access key | `string` | `""` | no |
| <a name="input_rancher_api_url"></a> [rancher\_api\_url](#input\_rancher\_api\_url) | Rancher API endpoint to manager your Harvester cluster | `string` | `""` | no |
| <a name="input_rancher_insecure"></a> [rancher\_insecure](#input\_rancher\_insecure) | Allow insecure connections to the Rancher API | `bool` | `false` | no |
| <a name="input_rancher_secret_key"></a> [rancher\_secret\_key](#input\_rancher\_secret\_key) | Rancher secret key | `string` | `""` | no |
| <a name="input_spot_instance"></a> [spot\_instance](#input\_spot\_instance) | Set to true to use spot instance instead of on demand. Also set you max bid price if true. | `bool` | `false` | no |
| <a name="input_ssh_key"></a> [ssh\_key](#input\_ssh\_key) | Your ssh key, examples: 'github: myghid' or 'ssh-rsa AAAAblahblah== keyname' | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_harvester_url"></a> [harvester\_url](#output\_harvester\_url) | n/a |
| <a name="output_join_ips"></a> [join\_ips](#output\_join\_ips) | n/a |
| <a name="output_seed_ip"></a> [seed\_ip](#output\_seed\_ip) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
